class TransformerView extends CircuitElementBaseView {
    private String _functionalityLabel;

    public TransformerView(Transformer element, int centerX, int centerY, String functionalityLabel) {
        super(element, centerX, centerY);
        _functionalityLabel = functionalityLabel;
    }

    public void draw() {
        setupDraw();
        drawLeftWinding();
        drawRightWinding();
        drawCore();
        drawName();
        drawUsageLabel();
    }

    private void drawLeftWinding() {
        noFill();
        int leftWindingCenterX = _centerX - 10;
        arc(leftWindingCenterX, _centerY - 12, 6, 6, -HALF_PI, HALF_PI);
        arc(leftWindingCenterX, _centerY, 6, 6, -HALF_PI, HALF_PI);
        arc(leftWindingCenterX, _centerY + 12, 6, 6, -HALF_PI, HALF_PI);

        fill(0xFFFFFFFF);
        circle(leftWindingCenterX, _centerY - 18, 3);
        circle(leftWindingCenterX, _centerY + 18, 3);

        fill(NO_CURRENT_COLOR);
        textSize(10);
        text('7', _centerX - 18, _centerY - 6);
        text('6', _centerX - 18, _centerY + 15);
    }

    private void drawRightWinding() {
        noFill();
        int rightWindingCenterX = _centerX + 10;
        arc(rightWindingCenterX, _centerY - 12, 6, 6, HALF_PI, PI + HALF_PI);
        arc(rightWindingCenterX, _centerY, 6, 6, HALF_PI, PI + HALF_PI);
        arc(rightWindingCenterX, _centerY + 12, 6, 6, HALF_PI, PI + HALF_PI);

        fill(0xFFFFFFFF);
        circle(rightWindingCenterX, _centerY - 18, 3);
        circle(rightWindingCenterX, _centerY + 18, 3);

        fill(NO_CURRENT_COLOR);
        textSize(10);
        text('5', _centerX + 12, _centerY - 6);
        text('1', _centerX + 12, _centerY + 15);
    }

    private void drawCore() {
        line(_centerX, _centerY - 18, _centerX, _centerY + 18);
    }

    private void drawName() {
        textSize(12);
        text("СТ-5", _centerX - 14, _centerY + 36);
    }

    private void drawUsageLabel() {
        int labelLetterCount = _functionalityLabel.length();
        text(_functionalityLabel, _centerX - (3 * labelLetterCount), _centerY - 24);   
    }
}
