// электрическая схема
// хранит все элементы и их соединения
// пока здесь вручную располагаются и соединяются элементы, в будущем лучше загружать расположение и соединения из файла
class Schematic {
    private ArrayList<CircuitElement> _circuitElements;
    private ArrayList<CircuitElementBaseView> _circuitElementViews;
    private ArrayList<CheckableCircuitElementBaseView> _checkableElementViews;
    private ArrayList<Battery> _batteries;
    private ArrayList<Joint> _joints;
    private ArrayList<CheckButton> _buttons;
    private EntranceTrafficLight trafficLight;

    public Schematic() {
        _circuitElements = new ArrayList<CircuitElement>();
        _circuitElementViews = new ArrayList<CircuitElementBaseView>();
        _checkableElementViews = new ArrayList<CheckableCircuitElementBaseView>();
        _batteries = new ArrayList<Battery>();
        _joints = new ArrayList<Joint>();
        _buttons = new ArrayList<CheckButton>();
    }

    public void Load() {
        Lamp topYellowLamp = loadFirstYellowLoop();
        Lamp greenLamp = loadGreenLoop();
        trafficLight = new EntranceTrafficLight(topYellowLamp, greenLamp, 80, 400);
    }

    public void update() {
        resetCurrent();
        startCurrentPathDiscovery();
        passCurrent();
    }

    public void draw(){
        for (CircuitElementBaseView elementView: _circuitElementViews)
            elementView.draw();
        for (CheckButton button: _buttons)
            button.draw();
        trafficLight.draw();
    }

    public void processMousePress() {
        for (CheckableCircuitElementBaseView elementView: _checkableElementViews)
            elementView.tryInverseCheckedState();
        for (CheckButton button: _buttons)
            button.press();
    }

    private void resetCurrent() {
        for (CircuitElement element: _circuitElements)
            element.resetCurrent();
        for (Joint joint: _joints)
            joint.resetCurrent();
    }

    private void startCurrentPathDiscovery() {
        for (Battery battery: _batteries) {
            battery.startCurrentPathDiscovery();
        }
    }

    private void passCurrent() {
        for (Battery battery: _batteries) {
            battery.passCurrent();
        }
    }

    private Lamp loadFirstYellowLoop() {
        final int lampCenterX = 180;
        final int lampCenterY = 80;

        Lamp yellowLamp = new Lamp();
        _circuitElements.add(yellowLamp);
        LampYellowView lampView = new LampYellowView(yellowLamp, lampCenterX, lampCenterY);
        _circuitElementViews.add(lampView);

        // верхний левый угол проводов
        Wire wire1 = new Wire();
        _circuitElements.add(wire1);
        WireView wireView1 = new WireView(wire1, lampCenterX, lampCenterY - 10 - 4, 8, true);
        _circuitElementViews.add(wireView1);

        Wire wire2 = new Wire();
        _circuitElements.add(wire2);
        WireView wireView2 = new WireView(wire2, lampCenterX + 60, lampCenterY - 18, 120, false);
        _circuitElementViews.add(wireView2);
        _checkableElementViews.add(wireView2);

        // нижний левый угол проводов до трансформатора
        Wire wire3 = new Wire();
        _circuitElements.add(wire3);
        WireView wireView3 = new WireView(wire3, lampCenterX, lampCenterY + 14, 8, true);
        _circuitElementViews.add(wireView3);

        Wire wire4 = new Wire();
        _circuitElements.add(wire4);
        WireView wireView4 = new WireView(wire4, lampCenterX + 155, lampCenterY + 18, 310, false);
        _circuitElementViews.add(wireView4);
        _checkableElementViews.add(wireView4);

        // реле
        Switch yellowSwitch = new Switch();
        _circuitElements.add(yellowSwitch);
        Relay relay = new Relay(yellowSwitch);
        _circuitElements.add(relay);
        RelayView relayView = new RelayView(relay, lampCenterX + 155, lampCenterY - 18, "1ЖО");
        _circuitElementViews.add(relayView);
        _checkableElementViews.add(relayView);

        Wire wire5 = new Wire();
        _circuitElements.add(wire5);
        WireView wireView5 = new WireView(wire5, lampCenterX + 120, lampCenterY + 3, 15, true);
        _circuitElementViews.add(wireView5);
        Wire wire6 = new Wire();
        _circuitElements.add(wire6);
        WireView wireView6 = new WireView(wire6, lampCenterX + 155, lampCenterY + 1 + 10, 70, false);
        _circuitElementViews.add(wireView6);
        _checkableElementViews.add(wireView6);
        Wire wire7 = new Wire();
        _circuitElements.add(wire7);
        WireView wireView7 = new WireView(wire7, lampCenterX + 120 + 35 + 35, lampCenterY + 3, 15, true);
        _circuitElementViews.add(wireView7);

        // провод слева сверху от трансформатора
        Wire wire8 = new Wire();
        _circuitElements.add(wire8);
        WireView wireView8 = new WireView(wire8, lampCenterX + 120 + 35 + 35 + 60, lampCenterY - 18, 120, false);
        _circuitElementViews.add(wireView8);
        _checkableElementViews.add(wireView8);

        // провода справа от трансформатора
        Wire wire9 = new Wire();
        _circuitElements.add(wire9);
        WireView wireView9 = new WireView(wire9, lampCenterX + 320 + 10 + 155, lampCenterY - 18, 310, false);
        _circuitElementViews.add(wireView9);
        _checkableElementViews.add(wireView9);
        Wire wire10 = new Wire();
        _circuitElements.add(wire10);
        WireView wireView10 = new WireView(wire10, lampCenterX + 320 + 10 + 155, lampCenterY + 18, 310, false);
        _circuitElementViews.add(wireView10);
        _checkableElementViews.add(wireView10);

        // трансформатор
        Transformer transformer = new Transformer();
        _circuitElements.add(transformer);
        TransformerView transformerView = new TransformerView(transformer, lampCenterX + 120 + 35 + 35 + 60 + 60 + 10, lampCenterY, "1Ж");
        _circuitElementViews.add(transformerView);

        // невидимая батарея
        Battery battery = new Battery(false);
        _batteries.add(battery);

        // кнопка управления батареей
        CheckButton button = new CheckButton(battery, lampCenterX + 750, lampCenterY, 0xFFFFE600, "Напряжение");
        _buttons.add(button);

        // соединения
        JointFactory jointFactory = new JointFactory();

        Joint joint1 = jointFactory.createJoint(yellowLamp, 1, wire1, 2);
        _joints.add(joint1);

        Joint joint2 = jointFactory.createJoint(wire1, 1, wire2, 1);
        _joints.add(joint2);

        Joint joint3 = jointFactory.createJoint(yellowLamp, 2, wire3, 1);
        _joints.add(joint3);

        Joint joint4 = jointFactory.createJoint(wire3, 2, wire4, 1);
        _joints.add(joint4);

        Joint joint5 = jointFactory.createJoint(wire2, 2, relay, 21);
        _joints.add(joint5);

        Joint joint6 = jointFactory.createJoint(wire5, 1, relay, 62);
        _joints.add(joint6);

        Joint joint7 = jointFactory.createJoint(wire6, 1, wire5, 2);
        _joints.add(joint7);

        Joint joint8 = jointFactory.createJoint(wire6, 2, wire7, 2);
        _joints.add(joint8);

        Joint joint9 = jointFactory.createJoint(wire7, 1, relay, 41);
        _joints.add(joint9);

        Joint joint10 = jointFactory.createJoint(wire8, 1, relay, 82);
        _joints.add(joint10);

        Joint joint11 = jointFactory.createJoint(wire8, 2, transformer, 7);
        _joints.add(joint11);

        Joint joint12 = jointFactory.createJoint(wire4, 2, transformer, 6);
        _joints.add(joint12);

        Joint joint13 = jointFactory.createJoint(wire9, 1, transformer, 5);
        _joints.add(joint13);

        Joint joint14 = jointFactory.createJoint(wire10, 1, transformer, 1);
        _joints.add(joint14);

        Joint joint15 = jointFactory.createJoint(wire9, 2, battery, 1);
        _joints.add(joint15);

        Joint joint16 = jointFactory.createJoint(wire10, 2, battery, 2);
        _joints.add(joint16);

        return yellowLamp;
    }
    
    private Lamp loadGreenLoop() {
        final int lampCenterX = 180;
        final int lampCenterY = 160;

        Lamp greenLamp = new Lamp();
        _circuitElements.add(greenLamp);
        LampGreenView lampView = new LampGreenView(greenLamp, lampCenterX, lampCenterY);
        _circuitElementViews.add(lampView);

        // верхний левый угол проводов
        Wire wire1 = new Wire();
        _circuitElements.add(wire1);
        WireView wireView1 = new WireView(wire1, lampCenterX, lampCenterY - 10 - 4, 8, true);
        _circuitElementViews.add(wireView1);

        Wire wire2 = new Wire();
        _circuitElements.add(wire2);
        WireView wireView2 = new WireView(wire2, lampCenterX + 60, lampCenterY - 18, 120, false);
        _circuitElementViews.add(wireView2);
        _checkableElementViews.add(wireView2);

        // нижний левый угол проводов до трансформатора
        Wire wire3 = new Wire();
        _circuitElements.add(wire3);
        WireView wireView3 = new WireView(wire3, lampCenterX, lampCenterY + 14, 8, true);
        _circuitElementViews.add(wireView3);

        Wire wire4 = new Wire();
        _circuitElements.add(wire4);
        WireView wireView4 = new WireView(wire4, lampCenterX + 155, lampCenterY + 18, 310, false);
        _circuitElementViews.add(wireView4);
        _checkableElementViews.add(wireView4);

        // реле
        Switch yellowSwitch = new Switch();
        _circuitElements.add(yellowSwitch);
        Relay relay = new Relay(yellowSwitch);
        _circuitElements.add(relay);
        RelayView relayView = new RelayView(relay, lampCenterX + 155, lampCenterY - 18, "1ЖО");
        _circuitElementViews.add(relayView);
        _checkableElementViews.add(relayView);

        Wire wire5 = new Wire();
        _circuitElements.add(wire5);
        WireView wireView5 = new WireView(wire5, lampCenterX + 120, lampCenterY + 3, 15, true);
        _circuitElementViews.add(wireView5);
        Wire wire6 = new Wire();
        _circuitElements.add(wire6);
        WireView wireView6 = new WireView(wire6, lampCenterX + 155, lampCenterY + 1 + 10, 70, false);
        _circuitElementViews.add(wireView6);
        _checkableElementViews.add(wireView6);
        Wire wire7 = new Wire();
        _circuitElements.add(wire7);
        WireView wireView7 = new WireView(wire7, lampCenterX + 120 + 35 + 35, lampCenterY + 3, 15, true);
        _circuitElementViews.add(wireView7);

        // провод слева сверху от трансформатора
        Wire wire8 = new Wire();
        _circuitElements.add(wire8);
        WireView wireView8 = new WireView(wire8, lampCenterX + 120 + 35 + 35 + 60, lampCenterY - 18, 120, false);
        _circuitElementViews.add(wireView8);
        _checkableElementViews.add(wireView8);

        // провода справа от трансформатора
        Wire wire9 = new Wire();
        _circuitElements.add(wire9);
        WireView wireView9 = new WireView(wire9, lampCenterX + 320 + 10 + 155, lampCenterY - 18, 310, false);
        _circuitElementViews.add(wireView9);
        _checkableElementViews.add(wireView9);
        Wire wire10 = new Wire();
        _circuitElements.add(wire10);
        WireView wireView10 = new WireView(wire10, lampCenterX + 320 + 10 + 155, lampCenterY + 18, 310, false);
        _circuitElementViews.add(wireView10);
        _checkableElementViews.add(wireView10);

        // трансформатор
        Transformer transformer = new Transformer();
        _circuitElements.add(transformer);
        TransformerView transformerView = new TransformerView(transformer, lampCenterX + 120 + 35 + 35 + 60 + 60 + 10, lampCenterY, "1Ж");
        _circuitElementViews.add(transformerView);

        // невидимая батарея
        Battery battery = new Battery(false);
        _batteries.add(battery);

        // кнопка управления батареей
        CheckButton button = new CheckButton(battery, lampCenterX + 750, lampCenterY, 0xFF06CB04, "Напряжение");
        _buttons.add(button);

        // соединения
        JointFactory jointFactory = new JointFactory();

        Joint joint1 = jointFactory.createJoint(greenLamp, 1, wire1, 2);
        _joints.add(joint1);

        Joint joint2 = jointFactory.createJoint(wire1, 1, wire2, 1);
        _joints.add(joint2);

        Joint joint3 = jointFactory.createJoint(greenLamp, 2, wire3, 1);
        _joints.add(joint3);

        Joint joint4 = jointFactory.createJoint(wire3, 2, wire4, 1);
        _joints.add(joint4);

        Joint joint5 = jointFactory.createJoint(wire2, 2, relay, 21);
        _joints.add(joint5);

        Joint joint6 = jointFactory.createJoint(wire5, 1, relay, 62);
        _joints.add(joint6);

        Joint joint7 = jointFactory.createJoint(wire6, 1, wire5, 2);
        _joints.add(joint7);

        Joint joint8 = jointFactory.createJoint(wire6, 2, wire7, 2);
        _joints.add(joint8);

        Joint joint9 = jointFactory.createJoint(wire7, 1, relay, 41);
        _joints.add(joint9);

        Joint joint10 = jointFactory.createJoint(wire8, 1, relay, 82);
        _joints.add(joint10);

        Joint joint11 = jointFactory.createJoint(wire8, 2, transformer, 7);
        _joints.add(joint11);

        Joint joint12 = jointFactory.createJoint(wire4, 2, transformer, 6);
        _joints.add(joint12);

        Joint joint13 = jointFactory.createJoint(wire9, 1, transformer, 5);
        _joints.add(joint13);

        Joint joint14 = jointFactory.createJoint(wire10, 1, transformer, 1);
        _joints.add(joint14);

        Joint joint15 = jointFactory.createJoint(wire9, 2, battery, 1);
        _joints.add(joint15);

        Joint joint16 = jointFactory.createJoint(wire10, 2, battery, 2);
        _joints.add(joint16);

        return greenLamp;
    }
}
