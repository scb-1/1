class LampView extends CircuitElementBaseView {
    private static final int LAMP_RADIUS = 20;

    private final float lineEndShift = LAMP_RADIUS * sqrt(2) / 2;

    public LampView(Lamp lamp, int centerX, int centerY) {
        super(lamp, centerX, centerY);
    }

    public void draw() {
        super.setupDraw();
        fill(0xFFFFFFFF);
        circle(_centerX, _centerY, LAMP_RADIUS);
        line(_centerX - lineEndShift + 1, _centerY - lineEndShift + 1, _centerX + lineEndShift, _centerY + lineEndShift);
        line(_centerX + lineEndShift - 1, _centerY - lineEndShift + 1, _centerX - lineEndShift + 1, _centerY + lineEndShift - 1);
    }
}
