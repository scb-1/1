// базовый класс для всех электрических элементов
// задает логику по передаче и приему тока
abstract class CircuitElement {
    private final HashMap<Integer, Joint> _joints; // список соединений, в которых участвует данный элемент. Типом Integer определяется № контакта (вывода) у элемента

    protected boolean _isCurrentPathSearching; // запущен ли поиск пути для протекания тока
    protected boolean _isCurrentPassed; // протекает ли ток через данный элемент

    protected CircuitElement() {
        _joints = new HashMap<Integer, Joint>();
    }

    // Принимает ток (или его отсутствие) на контакт текущего элемента. Просто рассылает ток дальше по соединениям
    public void receiveCurrent(int elementContact, boolean isCurrentPassed) {
        _isCurrentPassed = isCurrentPassed;
        Iterable<Integer> contacts = _joints.keySet();
        for (Integer contact: contacts) {
            if (contact == elementContact) // подумать: может быть подать ток ещё и на elementContact? но это плохая идея, если к этому контакту присоединена батарея
                continue;

            Joint joint = _joints.get(contact);
            joint.distributeCurrent(this, isCurrentPassed);
        }
    }

    public boolean isCurrentPassed() {
        return _isCurrentPassed;
    }

    // сбрасывает ток на элементе
    public void resetCurrent() {
        _isCurrentPassed = false;
        _isCurrentPathSearching = false; 
    }

    // в будущем метод будет переопреден в других элементах. Пока считается, что каждый элемент имеет два контакта с номерами 1 и 2
    public boolean isContactPresented(int contact) {
        return contact == 1 || contact == 2;
    }

    public void addJoint(int contact, Joint joint) {
        if (_joints.containsKey(contact))
            throw new IllegalStateException("Joint is already on contact: " + contact);

        _joints.put(contact, joint);
    }

    // функция узнает, пройдет ли ток через все соединения
    public boolean isCurrentPassable(int sourceContact) {
        if (_joints.size() == 0)
            return false;

        // если попали сюда, и _isCurrentPathSearching == true, то значит в схеме есть параллельное соединение. Возвращаем false, чтобы параллельное соединение искало другой путь для тока, чтобы в коде не было бесконечной рекурсии.
        if (_isCurrentPathSearching)
            return false;

        _isCurrentPathSearching = true;

        boolean isCurrentPassable = true;
        for (int contact: _joints.keySet()) {
            if (contact == sourceContact)
                continue;

            Joint joint = _joints.get(contact);
            if (!joint.isCurrentPassable(this)) {
                isCurrentPassable = false;
                break;
            }
        }

        _isCurrentPathSearching = false;
        return isCurrentPassable;
    }

    // передает ток (или сбрасывает его) на контакт
    protected void passCurrentToContact(int contact, boolean isCurrentPassed) {
        Joint targetJoint = _joints.get(contact);
        targetJoint.distributeCurrent(this, isCurrentPassed);
    }

    protected Joint getJoint(int contact) {
        return _joints.get(contact);
    }
}
