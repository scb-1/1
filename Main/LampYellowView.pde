class LampYellowView extends CircuitElementBaseView {
    private static final int LAMP_RADIUS = 10;

    public LampYellowView(Lamp lamp, int centerX, int centerY) {
        super(lamp, centerX, centerY);
    }

    public void draw() {
        super.setupDraw();
        fill(0xFFFFFFFF);
        circle(_centerX, _centerY, LAMP_RADIUS);
        line(_centerX + LAMP_RADIUS * 0.2, _centerY - LAMP_RADIUS, _centerX - LAMP_RADIUS, _centerY + LAMP_RADIUS * 0.2);
        line(_centerX + LAMP_RADIUS * 0.7, _centerY - LAMP_RADIUS * 0.7, _centerX - LAMP_RADIUS * 0.7, _centerY + LAMP_RADIUS * 0.7);
        line(_centerX + LAMP_RADIUS, _centerY - LAMP_RADIUS * 0.2, _centerX - LAMP_RADIUS * 0.2, _centerY + LAMP_RADIUS);
    }
}
