// соединение нескольких элементов 
class Joint {
    private final HashMap<CircuitElement, Integer> _elementsContacts; // список контактов элементов, которые участвуют в данном соединении 
    private final HashMap<CircuitElement, Integer> _passableElementsContacts;

    private boolean _isCurrentPresented; // присутствует ли ток в данном соединении
    private boolean _isCurrentPathSearching;

    public Joint() {
        _elementsContacts = new HashMap<CircuitElement, Integer>();
        _passableElementsContacts = new HashMap<CircuitElement, Integer>();
    }

    // присоединяет элемент к данному соединению
    public void attachCircuitElement(CircuitElement attachingElement, int attachingElementContact) {
        if (!attachingElement.isContactPresented(attachingElementContact))
            throw new IllegalStateException("Element doesn't have such contact: " + attachingElementContact);

        if (_elementsContacts.containsKey(attachingElement))
            throw new IllegalStateException("Element is already added to joint.");

        _elementsContacts.put(attachingElement, attachingElementContact);
        attachingElement.addJoint(attachingElementContact, this);
    }

    // распределяет ток (или сбрасывает его) по всем элементам, кроме того, с которого ток пришел
    public void distributeCurrent(CircuitElement sourceElement, boolean isCurrentPassed) {
        if (isCurrentPassed == _isCurrentPresented)
            return;

        _isCurrentPresented = isCurrentPassed;

        Iterable<CircuitElement> sinkElements = _passableElementsContacts.keySet();
        for (CircuitElement sinkElement: sinkElements) {
            if (sinkElement == sourceElement)
                continue;

            int sinkContact = _passableElementsContacts.get(sinkElement);
            sinkElement.receiveCurrent(sinkContact, isCurrentPassed);
        }
    }

    // сбрасывает ток и забывает элементы, через которые может пройти ток. В дальнейшем нужно вызывать isCurrentPassable
    public void resetCurrent() {
        _isCurrentPresented = false;
        _passableElementsContacts.clear();
    }

    public boolean isCurrentPresented() {
        return _isCurrentPresented;
    }

    // Проверяет, пройдет ли ток через элементы, которые участвуют в текущем соединении
    public boolean isCurrentPassable(CircuitElement sourceElement) {
        if (_isCurrentPathSearching)
            return false; // сюда попадем, если в схеме есть параллельное соединение

        _isCurrentPathSearching = true;
        _passableElementsContacts.clear();
        boolean isCurrentPassable = false;
        for (CircuitElement element: _elementsContacts.keySet()) {
            if (element == sourceElement)
                continue;

            int elementContact = _elementsContacts.get(element);
            boolean isElementPassable = element.isCurrentPassable(elementContact);
            if (isElementPassable) {
                isCurrentPassable = true;
                _passableElementsContacts.put(element, elementContact);
                int sourceElementContact = _elementsContacts.get(sourceElement);
                _passableElementsContacts.put(sourceElement, sourceElementContact);
            }
        }

        _isCurrentPathSearching = false;
        return isCurrentPassable;
    }
}
