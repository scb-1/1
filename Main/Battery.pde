class Battery extends CheckableCircuitElement {
    public Battery(boolean isEnabledInitially) {
        super();
        _isCurrentPassed = isEnabledInitially;
        _isChecked = !isEnabledInitially;
    }

    // запускает опрос всех последующих элементов и соединений на возможность протекания тока
    public void startCurrentPathDiscovery() {
        if (_isChecked)
            return;

        super.isCurrentPassable(1);
    }

    public boolean isCurrentPassable(int sourceContact) {
        if (_isChecked)
            return false;

        // если была вызвана функция startCurrentPathDiscovery, то _isCurrentPathSearching должно быть true. Возвращаем true, т.к. батарея всегда пропускает ток (исключение - когда батарея была нажата)
        if (_isCurrentPathSearching)
            return true;

        // просто так функцию isCurrentPassable нельзя вызывать. Она должна вызваться автоматически после вызова startCurrentPathDiscovery (плохая архитектура, подумать над исправлением)
        throw new IllegalStateException("Current path discovery is not started by battery.startCurrentPathDiscovery()");
    }

    // если честно, костыльный метод. Нужен чтобы один раз включить батарею
    public void passCurrent() {
        if (_isChecked)
            return;

        super.receiveCurrent(1, true);
    }

    public void receiveCurrent(int elementContact, boolean isCurrentPassed) {
    }

    protected void handleCheckedState(boolean isChecked) {
        if (_isChecked == isChecked)
            return;

        _isChecked = isChecked;
        _isCurrentPassed = !isChecked;
    }
}
