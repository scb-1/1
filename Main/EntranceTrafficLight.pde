// Входной светофор
// Его нужно связать с лампами, которые будут использоваться на схеме
class EntranceTrafficLight {
    private final int DISABLED_LAMP_COLOR = 0xFF6F6F6E;
    private Lamp _topYellowLamp;
    private Lamp _greenLamp;

    private int _centerX;
    private int _centerY;

    public EntranceTrafficLight(Lamp topYellowLamp, Lamp greenLamp, int centerX, int centerY) {
        _topYellowLamp = topYellowLamp;
        _greenLamp = greenLamp;
        _centerX = centerX;
        _centerY = centerY;
    }

    public void draw() {
        drawRod();
        drawTopBigPanel();
        drawBottomBigPanel();
        drawWhitePanel();
        drawGreenPlate();
        drawLights();
    }

    private void drawRod() {
        fill(0xFF3E3E3E);
        rect(_centerX, _centerY, 10, 340);
    }

    private void drawTopBigPanel() {
        fill(0xFF000000);
        rect(_centerX, _centerY - 250, 60, 90, 60, 60, 60, 60);
    }

    private void drawBottomBigPanel() {
        fill(0xFF000000);
        rect(_centerX, _centerY - 70, 60, 90, 60, 60, 60, 60);
    }

    private void drawWhitePanel() {
        fill(0xFF000000);
        circle(_centerX, _centerY + 170, 40);
    }

    private void drawGreenPlate() {
    }

    private void drawLights() {
        int topYellowLampColor = _topYellowLamp.isCurrentPassed() ? 0xFFFFE600 : DISABLED_LAMP_COLOR;
        fill(topYellowLampColor);
        circle(_centerX, _centerY - 250 - 35, 20);

        int greenLampColor = _greenLamp.isCurrentPassed() ? 0xFF06CB04 : DISABLED_LAMP_COLOR;
        fill(greenLampColor);
        circle(_centerX, _centerY - 250 + 35, 20);

        int redLampColor = DISABLED_LAMP_COLOR;
        fill(redLampColor);
        circle(_centerX, _centerY - 70 - 35, 20);

        int bottomYellowLampColor = DISABLED_LAMP_COLOR;
        fill(bottomYellowLampColor);
        circle(_centerX, _centerY - 70 + 35, 20);

        int whiteLampColor = DISABLED_LAMP_COLOR;
        fill(whiteLampColor);
        circle(_centerX, _centerY + 170, 20);
    }
}
