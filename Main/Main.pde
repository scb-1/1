Schematic schematic;

void setup()
{
    size(1000, 1000);
    ellipseMode(RADIUS);    // этот режим оказался удобнее
    rectMode(RADIUS);    // этот режим оказался удобнее
    background(255);

    schematic = new Schematic();
    schematic.Load();
}

void draw() {
    background(255);
    schematic.update();
    schematic.draw();
}

void mousePressed() {
    schematic.processMousePress();
}
