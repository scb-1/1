// базовая логика для кликабельных элементов
// по умолчанию логика такая: если текущее состояние _isChecked == true, то ток не проходит через элемент, иначе проходит
abstract class CheckableCircuitElement extends CircuitElement {
    protected boolean _isChecked; // нажат ли элемент

    public void receiveCurrent(int elementContact, boolean isCurrentPassed) {
        if (_isChecked)
            return;

        super.receiveCurrent(elementContact, isCurrentPassed);
    }

    public boolean isChecked() {
        return _isChecked;
    }

    public void setCheckedState(boolean isChecked) {
        handleCheckedState(isChecked);
    }

    public void inverseCheckedState() {
        handleCheckedState(!_isChecked);
    }

    public boolean isCurrentPassable(int sourceContact) {
        if (_isChecked)
            return false;

        return super.isCurrentPassable(sourceContact);
    }

    protected void handleCheckedState(boolean isChecked) {
        if (_isChecked == isChecked)
            return;

        _isChecked = isChecked;

        if (_isChecked) {
            _isCurrentPassed = false;
            return;
        }
    }
}
