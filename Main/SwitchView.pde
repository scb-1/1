class SwitchView extends CheckableCircuitElementBaseView {
    private static final int WIDTH = 40;

    public SwitchView(Switch switchElement, int centerX, int centerY) {
        super(switchElement, centerX, centerY);
    }

    public void draw() {
        super.setupDraw();

        line(_centerX - 15, _centerY - 20, _centerX - 15, _centerY - 10);
        line(_centerX + 20, _centerY - 20, _centerX + 20, _centerY - 10);

        float contactEndy = _checkableElement.isChecked() ? _centerY + 20 : _centerY - 10;
        line(_centerX + 20, _centerY - 10, _centerX - 20, contactEndy);
    }

    protected int getHeight() {
        return WIDTH;
    }

    protected int getWidth() {
        return WIDTH; 
    }
}
