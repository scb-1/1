abstract class CheckableCircuitElementBaseView extends CircuitElementBaseView {
    protected CheckableCircuitElement _checkableElement;

    protected CheckableCircuitElementBaseView(CheckableCircuitElement checkableElement, int centerX, int centerY) {
        super(checkableElement, centerX, centerY);
        _checkableElement = checkableElement;
    }

    public void tryInverseCheckedState() {
        if (!isMouseOver())
            return;

        _checkableElement.inverseCheckedState();
    }

    protected abstract int getHeight();
    protected abstract int getWidth();

    protected boolean isMouseOver() {
        int halfHeight = getHeight() / 2;
        int halfWidth = getWidth() / 2;

        boolean isMouseXOver = _centerX - halfWidth <= mouseX && mouseX <= _centerX + halfWidth;
        boolean isMouseYOver = _centerY - halfHeight <= mouseY && mouseY <= _centerY + halfHeight;

        return isMouseXOver && isMouseYOver;
    }
}
