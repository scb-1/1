// Трансформатор
// Подает на вторую обмотку ток, если он пришел на первую обмотку, и наоборот
class Transformer extends CircuitElement {
    private boolean _isParallelCurrentPathDiscoveryStarted;

    public void receiveCurrent(int sourceContact, boolean isCurrentPassed) {
        if (_isCurrentPassed == isCurrentPassed)
            return;

        _isCurrentPassed = isCurrentPassed;

        if (sourceContact == 5 || sourceContact == 7) {
            passCurrentToContact(1, isCurrentPassed);
            passCurrentToContact(6, isCurrentPassed);
            if (sourceContact == 5)
                passCurrentToContact(7, isCurrentPassed);
            else
                passCurrentToContact(5, isCurrentPassed);
        } else if (sourceContact == 1 || sourceContact == 6) {
            passCurrentToContact(5, isCurrentPassed);
            passCurrentToContact(7, isCurrentPassed);
            if (sourceContact == 1)
                passCurrentToContact(6, isCurrentPassed);
            else
                passCurrentToContact(1, isCurrentPassed);
        }
    }

    public boolean isContactPresented(int contact) {
        return contact == 5 || contact == 1 || contact == 7 || contact == 6;
    }

    // проверяет, пройдет ли ток через определенное соединение
    public boolean isCurrentPassable(int sourceContact) {
        int targetContact;
        int parallelTargetContact;
        if (sourceContact == 1) {
            targetContact = 5;
            parallelTargetContact = 7;
        } else if (sourceContact == 5) {
            targetContact = 1;
            parallelTargetContact = 6;
        } else if (sourceContact == 6) {
            targetContact = 7;
            parallelTargetContact = 5;
        } else if (sourceContact == 7) {
            targetContact = 6;
            parallelTargetContact = 1;
        } else {
            return false;
        }

        if (_isParallelCurrentPathDiscoveryStarted)
            return true;

        if (_isCurrentPathSearching)
            return false;

        _isCurrentPathSearching = true;
        Joint joint = getJoint(targetContact);
        boolean isCurrentPassable = joint.isCurrentPassable(this);

        // т.к. в трансформаторе два пути протекания тока (две обмотки), то и на параллельном пути нужно узнать, можно ли подать ток
        if (isCurrentPassable)
            startCurrentPathDiscovery(parallelTargetContact);

        _isCurrentPathSearching = false;
        return isCurrentPassable;
    }

    // запускает процесс построения пути для тока через определенный контакт
    private void startCurrentPathDiscovery(int targetContact) {
        _isParallelCurrentPathDiscoveryStarted = true;

        Joint targetJoint = getJoint(targetContact);
        targetJoint.isCurrentPassable(this);

        _isParallelCurrentPathDiscoveryStarted = false;
    }
}
