// Класс для более удобного создания соединений
class JointFactory {
    public Joint createJoint(CircuitElement firstElement, int firstElementContact, CircuitElement secondElement, int secondElementContact) {
        Joint joint = new Joint();
        joint.attachCircuitElement(firstElement, firstElementContact);
        joint.attachCircuitElement(secondElement, secondElementContact);
        return joint;
    }
}
