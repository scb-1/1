// базовый класс для графического представления элемента
// сам ничего не рисует, рисованием занимаются наследники класса
abstract class CircuitElementBaseView {
    protected static final int NO_CURRENT_COLOR = 0xFF000000;
    protected static final int CURRENT_COLOR = 0xFF0000FF;

    protected final CircuitElement _circuitElement;
    protected final int _centerX;
    protected final int _centerY;

    public abstract void draw();

    protected CircuitElementBaseView(CircuitElement circuitElement, int centerX, int centerY) {
        _circuitElement = circuitElement;
        _centerX = centerX;
        _centerY = centerY;
    }

    protected void setupDraw() {
        boolean isCurrentPassed = _circuitElement.isCurrentPassed();
        stroke(isCurrentPassed ? CURRENT_COLOR : NO_CURRENT_COLOR);
        strokeWeight(isCurrentPassed ? 2 : 1);
    }
}
