class WireView extends CheckableCircuitElementBaseView {
    private static final int WIRE_THICKNESS = 10;

    private int _wireLength;
    private boolean _isVertical;

    public WireView(Wire wire, int centerX, int centerY, int wireLength, boolean isVertical) {
        super(wire, centerX, centerY);
        _wireLength = wireLength;
        _isVertical = isVertical;
    }

    public void draw() {
        super.setupDraw();
        drawLine();

        if (_checkableElement.isChecked())
            drawCross();
    }

    protected int getHeight() {
        return _isVertical ? _wireLength : WIRE_THICKNESS;
    }

    protected int getWidth() {
        return _isVertical ? WIRE_THICKNESS : _wireLength;
    }

    private void drawLine() {
        int startX = _isVertical ? _centerX : _centerX - _wireLength / 2;
        int startY = _isVertical ? _centerY - _wireLength / 2 : _centerY;
        int endX = _isVertical ? _centerX : _centerX + _wireLength / 2;
        int endY = _isVertical ? _centerY + _wireLength / 2 : _centerY;

        line(startX, startY, endX, endY); 
    }

    private void drawCross() {
        fill(0xFFFFFFFF);
        noStroke();
        rect(_centerX, _centerY, 5, 5);

        stroke(0xFFFF0000);
        strokeWeight(2);
        line(_centerX - 5, _centerY - 5, _centerX + 5, _centerY + 5);
        line(_centerX + 5, _centerY - 5, _centerX - 5, _centerY + 5);
    }
}
