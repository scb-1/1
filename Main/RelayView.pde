class RelayView extends CheckableCircuitElementBaseView {
    private final int CIRCLE_RADIUS = 20;
    private final int CONTACT_LENGTH = 15;

    private String _functionalityLabel;

    public RelayView(Relay relay, int centerX, int centerY, String functionalityLabel) {
        super(relay, centerX, centerY);
        _functionalityLabel = functionalityLabel;
    }

    public void draw() {
        setupDraw();
        drawContacts();
        fill(0xFFFFFFFF);
        circle(_centerX, _centerY, CIRCLE_RADIUS);
        drawDiode();
        drawText();

        if (_checkableElement.isChecked())
            drawCross();
    }

    protected int getHeight() {
        return CIRCLE_RADIUS * 2;
    }

    protected int getWidth() {
        return (CIRCLE_RADIUS + CONTACT_LENGTH) * 2;
    }

    private void drawDiode() {
        triangle(_centerX - 7, _centerY, _centerX + 6, _centerY - 10, _centerX + 6, _centerY + 10);
        line(_centerX - 7, _centerY - 10, _centerX - 7, _centerY + 10);
    }

    private void drawContacts() {
        line(_centerX - CIRCLE_RADIUS - CONTACT_LENGTH, _centerY, _centerX + CIRCLE_RADIUS + CONTACT_LENGTH, _centerY);
        line(_centerX - CIRCLE_RADIUS - CONTACT_LENGTH, _centerY + 13, _centerX + CIRCLE_RADIUS + CONTACT_LENGTH, _centerY + 13);
    }

    private void drawText() {
        fill(NO_CURRENT_COLOR);
        textSize(10);
        text("21", _centerX - CIRCLE_RADIUS - CONTACT_LENGTH + 2, _centerY - 5);
        text("82", _centerX + CONTACT_LENGTH + 7, _centerY - 5);
        text("62", _centerX - CIRCLE_RADIUS - CONTACT_LENGTH + 2, _centerY + CIRCLE_RADIUS + 4);
        text("41", _centerX + CONTACT_LENGTH + 7, _centerY + CIRCLE_RADIUS + 4);
        textSize(12);
        int labelLetterCount = _functionalityLabel.length();
        text(_functionalityLabel, _centerX - (4 * labelLetterCount), _centerY - CIRCLE_RADIUS - 2);   
    }

    private void drawCross() {
        stroke(0xFFFF0000);
        strokeWeight(3);
        line(_centerX - CIRCLE_RADIUS, _centerY - CIRCLE_RADIUS, _centerX + CIRCLE_RADIUS, _centerY + CIRCLE_RADIUS);
        line(_centerX + CIRCLE_RADIUS, _centerY - CIRCLE_RADIUS, _centerX - CIRCLE_RADIUS, _centerY + CIRCLE_RADIUS);
    }
}
