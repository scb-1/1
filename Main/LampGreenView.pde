class LampGreenView extends CircuitElementBaseView {
    private static final int LAMP_RADIUS = 10;

    public LampGreenView(Lamp lamp, int centerX, int centerY) {
        super(lamp, centerX, centerY);
    }

    public void draw() {
        super.setupDraw();
        fill(0xFFFFFFFF);
        circle(_centerX, _centerY, LAMP_RADIUS);
    }
}
