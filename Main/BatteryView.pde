class BatteryView extends CheckableCircuitElementBaseView {
    private static final int HALF_WIDTH = 20;

    private final boolean _isVertical;

    public BatteryView(Battery battery, int centerX, int centerY, boolean isVertical) {
        super(battery, centerX, centerY);
        _isVertical = isVertical;
    }

    public void draw() {
        super.setupDraw();
        drawAnode();
        drawCatode();
    }

    protected int getHeight() {
        return HALF_WIDTH * 2;
    }

    protected int getWidth() {
        return HALF_WIDTH * 2;
    }

    private void drawAnode() {
        if (_isVertical) {
            line(_centerX, _centerY - HALF_WIDTH, _centerX, _centerY - 3);
            line(_centerX - 6, _centerY - 3, _centerX + 6, _centerY - 3);
            return;
        }
        line(_centerX - HALF_WIDTH, _centerY, _centerX - 3, _centerY);
        line(_centerX - 3, _centerY - 6, _centerX - 3, _centerY + 6);
    }

    private void drawCatode() {
        if (_isVertical) {
            line(_centerX, _centerY + 3, _centerX, _centerY + HALF_WIDTH);
            line(_centerX - 15, _centerY + 3, _centerX + 15, _centerY + 3);
            return;
        }
        line(_centerX + 3, _centerY, _centerX + HALF_WIDTH, _centerY);
        line(_centerX + 3, _centerY - 15, _centerX + 3, _centerY + 15);
    }
}
