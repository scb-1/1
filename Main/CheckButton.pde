// Кнопка, которая может находиться в двух состояниях - нажата (_isChecked == true) и отжата (_isChecked == false).
// Она переключает связанный с ней интерактивный элемент (провод, реле и т.д.)
// Когда кнопка в нажатом состоянии, то связанный элемент пропускает ток, в отжатом состоянии - не пропускает
// Кнопке можно задать цвет
class CheckButton {
    private final int MIN_WIDTH = 60;    // минимальная ширина кнопки. Она может быть и шире, это зависит от текста внутри кнопки
    private final int HEIGHT = 30;
    private final int TEXT_SIZE = 14;
    private final int TEXT_COLOR = 0xFF000000;
    private final int TEXT_MARGIN = 20;    // отступ от левого и правого края кнопки до текста

    private CheckableCircuitElement _checkableElement;

    private int _centerX;
    private int _centerY;

    private int _initialColor;    // изначальный цвет кнопки (в отжатом состоянии)
    private int _checkedColor;    // цвет в нажатом состоянии
    private int _hoverColor;    // цвет, когда над кнопкой расположен курсор
    private int _pressedColor;    // цвет, когда кнопка нажата курсором

    private String _text;
    private int _width;
    private boolean _isChecked;    // состояние кнопки

    public CheckButton(CheckableCircuitElement checkableElement, int centerX, int centerY, String buttonText) {
        this(checkableElement, centerX, centerY, 0xFFEAEAEA, buttonText);
    }

    public CheckButton(CheckableCircuitElement checkableElement, int centerX, int centerY, int initialColor, String buttonText) {
        _checkableElement = checkableElement;
        _centerX = centerX;
        _centerY = centerY;
        _text = buttonText;
        _width = max(MIN_WIDTH, (int)textWidth(buttonText) + 2 * TEXT_MARGIN);
        _isChecked = !checkableElement.isChecked();
        initializeColors(initialColor);
    }

    public void press() {
        if (!isMouseOver())
            return;

        _isChecked = !_isChecked;
        _checkableElement.setCheckedState(!_isChecked);
    }

    public void draw() {
        stroke(TEXT_COLOR);
        strokeWeight(1);
        if (!isMouseOver()) {
            if (_isChecked)
                drawDownedButton(_checkedColor, 2);
            else {
                fill(_initialColor);
                rect(_centerX, _centerY, _width / 2, HEIGHT / 2, 4, 4, 4, 4);
            }
        } else if (mousePressed)
            drawDownedButton(_pressedColor, 2);
        else if (isMouseOver())
            drawDownedButton(_hoverColor, 1);

        textSize(TEXT_SIZE);
        fill(TEXT_COLOR);
        text(_text, _centerX - textWidth(_text) / 2, _centerY + TEXT_SIZE / 2);
    }

    private void initializeColors(int initialColor) {
        _initialColor = initialColor;
        _checkedColor = lerpColor(0xFF000000, initialColor, 0.6);
        _pressedColor = lerpColor(0xFF000000, initialColor, 0.3);
        _hoverColor = lerpColor(0xFF000000, initialColor, 0.75);
    }

    private boolean isMouseOver() {
        int halfHeight = HEIGHT / 2;
        int halfWidth = _width / 2;

        boolean isMouseXOver = _centerX - halfWidth <= mouseX && mouseX <= _centerX + halfWidth;
        boolean isMouseYOver = _centerY - halfHeight <= mouseY && mouseY <= _centerY + halfHeight;

        return isMouseXOver && isMouseYOver;
    }

    private void drawDownedButton(int buttonColor, int borderWidth) {
        fill(0xFF2F2F2F);
        rect(_centerX, _centerY, _width / 2, HEIGHT / 2, 4, 4, 4, 4);
        fill(buttonColor);
        rect(_centerX, _centerY, _width / 2 - borderWidth, HEIGHT / 2 - borderWidth, 4, 4, 4, 4);
    }
}
