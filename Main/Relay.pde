// Реле
// Его нужно связать с выключателем
class Relay extends CheckableCircuitElement {
    private Switch _controlledSwitch;
    private boolean _isParallelCurrentPathDiscoveryStarted;

    public Relay(Switch controlledSwitch) {
        super();
        _controlledSwitch = controlledSwitch;
    }

    public void receiveCurrent(int elementContact, boolean isCurrentPassed) {
        super.receiveCurrent(elementContact, isCurrentPassed);
        _controlledSwitch.setCheckedState(!_isCurrentPassed);
    }

    public void inverseCheckedState() {
        super.inverseCheckedState();
        _controlledSwitch.setCheckedState(!_isCurrentPassed);
    }

    public boolean isContactPresented(int contact) {
        return contact == 21 || contact == 82 || contact == 62 || contact == 41;
    }

    // проверяет, пройдет ли ток через определенное соединение
    public boolean isCurrentPassable(int sourceContact) {
        if (_isChecked)
            return false;

        int targetContact;
        int parallelTargetContact;
        if (sourceContact == 21) {
            targetContact = 82;
            parallelTargetContact = 41;
        } else if (sourceContact == 82) {
            targetContact = 21;
            parallelTargetContact = 62;
        } else if (sourceContact == 62) {
            targetContact = 41;
            parallelTargetContact = 82;
        } else if (sourceContact == 41) {
            targetContact = 62;
            parallelTargetContact = 21;
        } else {
            return false;
        }

        if (_isParallelCurrentPathDiscoveryStarted)
            return true;

        if (_isCurrentPathSearching)
            return false;

        _isCurrentPathSearching = true;
        Joint joint = getJoint(targetContact);
        boolean isCurrentPassable = joint.isCurrentPassable(this);

        // т.к. в реле два пути протекания тока, то и на параллельном пути нужно узнать, можно ли подать ток
        if (isCurrentPassable)
            startCurrentPathDiscovery(parallelTargetContact);

        _isCurrentPathSearching = false;
        return isCurrentPassable;
    }

    // запускает процесс построения пути для тока через определенный контакт
    private void startCurrentPathDiscovery(int targetContact) {
        _isParallelCurrentPathDiscoveryStarted = true;

        Joint targetJoint = getJoint(targetContact);
        targetJoint.isCurrentPassable(this);

        _isParallelCurrentPathDiscoveryStarted = false;
    }
}
